from os import environ

from channels.routing import get_default_application

from django import setup


environ.setdefault('DJANGO_SETTINGS_MODULE', 'CRM.settings')

setup()

application = get_default_application()
