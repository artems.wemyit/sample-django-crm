import threading

from django.contrib.admin.models import ADDITION, CHANGE, DELETION, LogEntry
from django.contrib.auth.models import AnonymousUser
from django.contrib.contenttypes.models import ContentType
from django.core.exceptions import PermissionDenied
from django.db.models.signals import post_save, post_delete
from django.dispatch import receiver
from django.urls import resolve
from django.urls.resolvers import URLResolver, URLPattern


_thread_locals = threading.local()

DISABLE_LOGGING_VIEWS = set()

DISABLE_LOGGING_ATTRIBUTE = '_disable_logging'


def disable_model_logging(cls):
    setattr(cls, DISABLE_LOGGING_ATTRIBUTE, True)
    return cls


disable_model_logging(LogEntry)


def get_user_from_threadlocal():
    return getattr(_thread_locals, 'user', None)


def _disable_logging_url_pattern(pattern):
    if not isinstance(pattern, URLPattern):
        raise AttributeError("Unexpected value")
    DISABLE_LOGGING_VIEWS.add(pattern.callback)


def patch_pattern_to_prevent_logging(url_instance):
    if isinstance(url_instance, URLPattern):
        _disable_logging_url_pattern(url_instance)
    if isinstance(url_instance, URLResolver):
        for pattern in url_instance.url_patterns:
            patch_pattern_to_prevent_logging(pattern)
    return url_instance


def patch_url_to_prevent_logging(urls):
    for patterns in urls:
        for pattern in patterns:
            patch_pattern_to_prevent_logging(pattern)
    return urls


class AuditLoggingMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        _thread_locals.user = None
        if resolve(request.path).func not in DISABLE_LOGGING_VIEWS:
            _thread_locals.user = getattr(request, 'user', AnonymousUser())

        response = self.get_response(request)
        _thread_locals.user = None
        return response


class ModelsChangeLogging:
    @staticmethod
    def get_content_type(obj):
        return ContentType.objects.get_for_model(
            obj, for_concrete_model=False).id

    def log_model(self, obj, is_created, message=''):
        if is_created:
            self.log_addition(obj, message)
        self.log_change(obj, message)

    def log_addition(self, obj, message=''):
        self.create_log(obj=obj, log_type=ADDITION, message=message)

    def log_change(self, obj, message=''):
        self.create_log(obj=obj, log_type=CHANGE, message=message)

    def log_deletion(self, obj, message=''):
        self.create_log(obj=obj, log_type=DELETION, message=message)

    def create_log(self, obj, log_type, message):
        user = get_user_from_threadlocal()
        if isinstance(user, AnonymousUser):
            raise PermissionDenied('Unauthorized request.')
        if user is None:
            return
        LogEntry.objects.log_action(
            user_id=user.id,
            content_type_id=self.get_content_type(obj),
            object_id=obj.id,
            object_repr=str(obj),
            action_flag=log_type,
            change_message=message
        )


action_logger = ModelsChangeLogging()


def should_be_logged(instance):
    return not getattr(instance, DISABLE_LOGGING_ATTRIBUTE, False)


@receiver(post_save)
def log_model_action(instance, created, **kwargs):
    if should_be_logged(instance):
        action_logger.log_model(instance, is_created=created)


@receiver(post_delete)
def log_model_deletion(instance, **kwargs):
    if should_be_logged(instance):
        action_logger.log_deletion(instance)
