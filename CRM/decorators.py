from functools import wraps


def with_attrs(**func_attrs):
    """
    Decorator.
    Set attributes in the decorated function at definition time.
    Accepts keyword arguments only.
    """
    def attr_decorator(function):

        @wraps(function)
        def wrapper(*args, **kwargs):
            return function(*args, **kwargs)

        for attr, value in func_attrs.items():
            setattr(wrapper, attr, value)

        return wrapper

    return attr_decorator
