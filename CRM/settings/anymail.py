from os import getenv

# Anymail settings
# Use 'api.eu.mailgun.net/v3' for EU in MAILGUN_API_URL
ANYMAIL = {
    "MAILGUN_API_KEY": getenv('MAILGUN_API_KEY'),
    "MAILGUN_SENDER_DOMAIN": getenv('MAILGUN_SENDER_DOMAIN'),
    "MAILGUN_API_URL": "https://api.mailgun.net/v3",
    'WEBHOOK_SECRET': getenv('MAILGUN_WEBHOOK_SECRET'),
}

# General E-mail settings
EMAIL_HOST = 'smtp.mailgun.org'  # 'smtp.eu.mailgun.org' for EU hosting
EMAIL_PORT = 587
EMAIL_HOST_USER = '{0}@{1}'.format(
    getenv('MAILGUN_USER'), ANYMAIL.get('MAILGUN_SENDER_DOMAIN'))
EMAIL_HOST_PASSWORD = getenv('MAILGUN_PASSWORD')
EMAIL_USE_TLS = True
EMAIL_BACKEND = "anymail.backends.mailgun.EmailBackend"
DEFAULT_FROM_EMAIL = "no-reply@{}".format(ANYMAIL.get('MAILGUN_SENDER_DOMAIN'))
EMAIL_TOTAL_SIZE_MB = 24
EMAIL_TOTAL_SIZE_BYTES = 1024 * 1024 * EMAIL_TOTAL_SIZE_MB
