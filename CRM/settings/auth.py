# This project utilises custom User auth model
AUTH_USER_MODEL = 'Employee.User'

AUTHENTICATION_BACKENDS = [
    'django.contrib.auth.backends.ModelBackend',
]
