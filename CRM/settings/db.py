from os import getenv


DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': getenv('DB_NAME', 'crm'),
        'USER': getenv('DB_USER', 'wemyit'),
        'PASSWORD': getenv('DB_PASS', 'wemyit'),
        'HOST': getenv('DB_HOST', 'localhost'),
        'PORT': getenv('DB_PORT', '5432'),
    },
}
