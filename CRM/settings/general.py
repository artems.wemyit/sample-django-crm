"""
Django settings for CRM project.
"""

import os
from os import getenv

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(
    os.path.dirname(os.path.abspath(__file__))))

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = getenv('APP_KEY', 'test')

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = bool(getenv('APP_DEBUG'))

CHAT_URL = getenv('CHAT_URL', 'http://localhost:3000/')
DEBUG_TASKS = bool(getenv('DEBUG_TASKS'))
CHAT_USERNAME = 'Chat.Bot'

LOGIN_URL = '/login/'
LOGIN_REDIRECT_URL = '/'

ALLOWED_HOSTS = [x.strip() for x in getenv('APP_HOSTS', '*').split(',')]
INTERNAL_IPS = ['127.0.0.1', 'localhost', '0.0.0.0', '172.19.0.1']

# Backup settings
DBBACKUP_STORAGE = 'django.core.files.storage.FileSystemStorage'
DBBACKUP_STORAGE_OPTIONS = {'location': getenv('APP_BACKUP', '/CRM/backups')}

# Application definition
INSTALLED_APPS = [
    'channels',
    'dal',
    'dal_select2',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'anymail',
    'rest_framework',
    'tinymce',
    'webpack_loader',
    'dbbackup',
    'django_cleanup',
    'django_celery_beat',
    'Dashboard.apps.DashboardConfig',
    'Inventory.apps.InventoryConfig',
    'Employee.apps.EmployeeConfig',
    'Mail.apps.MailConfig',
    'Calendar.apps.CalendarConfig',
    'Vacation.apps.VacationConfig',
    'Tasks.apps.TasksConfig',
    'Chat.apps.ChatConfig',
    'Resource.apps.ResourceConfig'
]

ROOT_URLCONF = 'CRM.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [
            os.path.join(BASE_DIR, 'Calendar', 'templates'),
            os.path.join(BASE_DIR, 'Chat', 'templates'),
            os.path.join(BASE_DIR, 'Dashboard', 'templates'),
            os.path.join(BASE_DIR, 'Inventory', 'templates'),
            os.path.join(BASE_DIR, 'Mail', 'templates'),
        ],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
                'Chat.processors.chatroom_info',
                'Mail.processors.mailbox_info',
                'Dashboard.processors.dasboard_info'
            ],
            'libraries': {
                'filters': 'CRM.templates_extras.templates_filters',
            },
        },
    },
]

ASGI_APPLICATION = 'CRM.routing.application'

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': (
            'django.contrib.auth.password_validation.'
            'UserAttributeSimilarityValidator'
        )
    },
    {
        'NAME': (
            'django.contrib.auth.password_validation.'
            'MinimumLengthValidator'
        ),
    },
    {
        'NAME': (
            'django.contrib.auth.password_validation.'
            'CommonPasswordValidator'
        ),
    },
    {
        'NAME': (
            'django.contrib.auth.password_validation.'
            'NumericPasswordValidator'
        ),
    },
]


LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True

STATIC_URL = '/static/'
STATIC_ROOT = os.path.join(BASE_DIR, 'static')

MEDIA_URL = '/media/'
MEDIA_ROOT = os.path.join(BASE_DIR, 'media')
FIXTURE_DIRS = [os.path.join(BASE_DIR, 'CRM', 'assets')]

REST_FRAMEWORK = {
    'DEFAULT_RENDERER_CLASSES': (
        'rest_framework.renderers.JSONRenderer',
    ),
    'DEFAULT_PERMISSION_CLASSES': [
        'rest_framework.permissions.IsAuthenticated'
    ]
}


TINYMCE_DEFAULT_CONFIG = {
    'height': 360,
    'width': 'auto',
    'cleanup_on_startup': True,
    'custom_undo_redo_levels': 20,
    'selector': 'textarea',
    'theme': 'modern',
    'plugins': '''
            textcolor save link image media preview codesample contextmenu
            table code lists fullscreen  insertdatetime  nonbreaking
            contextmenu directionality searchreplace wordcount visualblocks
            visualchars code fullscreen autolink lists  charmap print  hr
            anchor pagebreak
            ''',
    'toolbar1': '''
            fullscreen preview bold italic underline | fontselect,
            fontsizeselect  | forecolor backcolor | alignleft alignright |
            aligncenter alignjustify | indent outdent | bullist numlist table |
            | link image media | codesample |
            ''',
    'toolbar2': '''
            visualblocks visualchars |
            charmap hr pagebreak nonbreaking anchor |  code |
            ''',
    'contextmenu': 'formats | link image',
    'menubar': True,
    'statusbar': True,
}

WEBPACK_LOADER = {
    'DEFAULT': {
        'CACHE': not DEBUG,
        'BUNDLE_DIR_NAME': '/',
        'STATS_FILE': os.path.join(STATIC_ROOT, 'webpack', 'stats.json'),
        'POLL_INTERVAL': 0.1,
        'TIMEOUT': None,
        'IGNORE': [r'.+\.hot-update.js', r'.+\.map']
    }
}

X_FRAME_OPTIONS = 'SAMEORIGIN'

PROBATION = {
    'DURATION': {
        'months': 2,
        'days': 1,
    },
    'EXPIRATION_CHECK_DAYS': 4
}

BUSINESS_WEEKDAYS = {
    'Monday': True,
    'Tuesday': True,
    'Wednesday': True,
    'Thursday': True,
    'Friday': True,
    'Saturday': False,
    'Sunday': False,
}

DEFAULT_AUTO_FIELD = 'django.db.models.AutoField'
CALENDAR_DATE_FORMAT = '%A, %d %B, %Y %H:%M'

FILE_STREAM_CHUNK_SIZE = 1024

GIPHY_TOKEN = os.getenv('GIPHY_API_KEY')
