import sentry_sdk
from sentry_sdk.integrations.django import DjangoIntegration
from sentry_sdk.integrations.celery import CeleryIntegration


sentry_sdk.init(
    integrations=[DjangoIntegration(), CeleryIntegration()],

    # Set traces_sample_rate to 1.0 to capture 100%
    # of transactions for performance monitoring.
    traces_sample_rate=0.5,

    # To associate users to errors enable sending PII data
    # (assuming you are using django.contrib.auth)
    send_default_pii=True,
)
