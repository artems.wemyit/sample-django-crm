import sys


if 'test' in sys.argv:
    AUTHENTICATION_BACKENDS = ['django.contrib.auth.backends.ModelBackend']

    CHANNEL_LAYERS = {
        "default": {"BACKEND": "channels.layers.InMemoryChannelLayer"},
        "testlayer": {"BACKEND": "channels.layers.InMemoryChannelLayer"},
    }

    MESSAGE_STORAGE = 'django.contrib.messages.storage.cookie.CookieStorage'
