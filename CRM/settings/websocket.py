from os import getenv


redis_host = getenv('REDIS_HOST', 'localhost')
redis_port = int(getenv('REDIS_PORT', '6379'))

CHANNEL_LAYERS = {
    "default": {
        "BACKEND": "channels_redis.core.RedisChannelLayer",
        "CONFIG": {"hosts": [(redis_host, redis_port)]}
    }
}
