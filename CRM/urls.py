from django.conf import settings
from django.conf.urls import include, url
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, re_path
from django.utils.translation import ugettext_lazy as _

from Dashboard.views import DashboardVacationView
from CRM.audit_utils import patch_url_to_prevent_logging
from Mail.consumers import MailConsumer

from CRM.views import MediaStreamView


admin.site.site_title = _("CRM")
admin.site.site_header = _("CRM Administration")
admin.site.index_title = _('Site administration')

urlpatterns = [
    url('admin/', patch_url_to_prevent_logging(admin.site.urls)),
    path('vacation/', DashboardVacationView.as_view(), name='vacation'),
    path('', include('Calendar.urls')),
    url(r'^anymail/', include('anymail.urls')),
    url(
        r'^api-auth/',
        include('rest_framework.urls', namespace='rest_framework'),
    ),
    url(r'^', include('Dashboard.urls')),
    url(r'^api/', include('Vacation.api_urls')),
    url(r'^api/', include('Inventory.api_urls')),
    url(r'^api/', include('Chat.api_urls')),
    url(r'^chat/', include('Chat.urls')),
    url(r'^mail/', include('Mail.urls')),
    url(r'^tinymce/', include('tinymce.urls')),
    path(
        'media/<str:folder>/<int:file_id>',
        MediaStreamView.as_view(),
        name='media_stream',
    ),
]
websocket_urlpatterns = [
    re_path(r'notifications/$', MailConsumer.as_asgi(), name='ws_stats'),
]

if settings.DEBUG:
    import debug_toolbar
    urlpatterns += static(
        settings.STATIC_URL,
        document_root=settings.STATIC_ROOT,
    )
    urlpatterns += static(
        settings.MEDIA_URL,
        document_root=settings.MEDIA_ROOT,
    )
    urlpatterns += [path('__debug__/', include(debug_toolbar.urls))]
