import mimetypes

from django.conf import settings
from django.contrib.auth.decorators import login_required
from django.http import Http404, StreamingHttpResponse
from django.shortcuts import get_object_or_404
from django.utils.decorators import method_decorator
from rest_framework.views import APIView

from Employee.models import User
from Inventory.models import Item
from Mail.models import EmailAttachment


mimetypes.init()


media_config = {
    'attachments': lambda obj_id, user:
        (
            get_object_or_404(
                EmailAttachment.objects
                .filter(message__mailbox__users__in=[user]), id=obj_id)
            .document
        ),
    'avatars': lambda obj_id, user: get_object_or_404(User, id=obj_id).photo,
    'qrcode': lambda obj_id, user: get_object_or_404(Item, id=obj_id).qrcode,
}


@method_decorator(login_required, name='dispatch')
class MediaStreamView(APIView):
    def get(self, request, folder, file_id):
        if folder not in media_config.keys():
            raise Http404

        file = media_config[folder](file_id, request.user)
        content_type = mimetypes.guess_type(file.path)[0]
        return StreamingHttpResponse(
            file.chunks(settings.FILE_STREAM_CHUNK_SIZE),
            content_type=content_type,
        )
