from uvicorn.workers import UvicornWorker


class CustomWorker(UvicornWorker):
    CONFIG_KWARGS = {
        'loop': 'uvloop',
        'http': 'httptools',
        'ws': 'wsproto',
        'lifespan': 'off',
        'interface': 'asgi3',
    }
