from django.contrib import admin

from Chat.models.chat_room import ChatRoom
from Chat.models.message import Message
from Chat.models.participant import Participant
from Chat.models.unread_message import UnreadMessage
from Chat.forms import MessageForm


@admin.register(ChatRoom)
class ChatRoomModelAdmin(admin.ModelAdmin):
    list_display = ('name',)
    readonly_fields = ('id',)


@admin.register(Message)
class MessageModelAdmin(admin.ModelAdmin):
    form = MessageForm
    fields = ('chat_room', 'sender', 'text')
    list_display = ('sender', 'chat_room', 'date')
    readonly_fields = ('id', 'chat_room', 'sender', 'text')

    def changeform_view(
        self,
        request,
        object_id=None,
        form_url='',
        extra_context=None
    ):
        extra_context = extra_context or {}
        extra_context['show_save_and_continue'] = False
        extra_context['show_save_and_add_another'] = False
        extra_context['show_save'] = False

        return super().changeform_view(
            request,
            object_id,
            extra_context=extra_context
        )


@admin.register(Participant)
class ParticipantModelAdmin(admin.ModelAdmin):
    list_display = ('person', 'chat_room')
    readonly_fields = ('id',)


@admin.register(UnreadMessage)
class UnreadMessageModelAdmin(admin.ModelAdmin):
    list_display = ('participant', 'message')
    readonly_fields = ('id', 'message', 'participant')

    def changeform_view(
        self,
        request,
        object_id=None,
        form_url='',
        extra_context=None
    ):
        extra_context = extra_context or {}
        extra_context['show_save_and_continue'] = False
        extra_context['show_save_and_add_another'] = False
        extra_context['show_save'] = False

        return super().changeform_view(
            request,
            object_id,
            extra_context=extra_context
        )
