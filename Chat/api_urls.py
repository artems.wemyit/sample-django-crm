from django.urls import path

from Chat import views

app_name = 'api-chat'

urlpatterns = [
    path(
        'chat/<uuid:chat_uuid>/messages',
        views.fetch_messages,
        name='fetch_messages'),
    path(
        'chat/<uuid:chat_uuid>/send',
        views.send_message,
        name='send_message')
]
