from django import forms
from django.conf import settings

from Chat.models.message import Message, MessageType
from Chat.models.participant import Participant
from Chat.models.unread_message import UnreadMessage


class FetchMessagesForm(forms.Form):
    message_type = forms.ChoiceField(
        choices=MessageType.choices,
        required=True)
    count = forms.IntegerField(
        min_value=settings.CHAT_CONFIGURATION['min_messages'],
        max_value=settings.CHAT_CONFIGURATION['max_messages'],
        required=True)
    before = forms.UUIDField(required=False)


class MessageForm(forms.ModelForm):
    class Meta:
        model = Message
        fields = ['chat_room', 'sender', 'text']

    def save(self, commit=True):
        message = super().save(commit)
        participants = (
            Participant.objects
            .filter(chat_room=message.chat_room_id)
            .exclude(person=message.sender_id))
        UnreadMessage.objects.bulk_create([
            UnreadMessage(message=message, participant=p)
            for p in participants])
        return message
