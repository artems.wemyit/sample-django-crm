import uuid

from django.db import models


class ChatRoom(models.Model):
    id = models.UUIDField(
        primary_key=True,
        default=uuid.uuid4)
    name = models.CharField(max_length=255)

    class Meta:
        db_table = 'chat_room'
        ordering = ('name',)

    def __str__(self):
        return self.name
