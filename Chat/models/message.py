import uuid
from datetime import datetime

from django.db import models

from django.db.models import Subquery
from django.db.models.functions import Now


class MessageQuerySet(models.query.QuerySet):
    def chat_messages(self, chat_uuid):
        """Returns all message for the corresponding chat"""
        return self.filter(chat_room=chat_uuid)

    def messages_before_message(self, before):
        """Returns messages with a date
         LESS than the date of the selected message"""
        date_qs = self.filter(id=before).values_list('date', flat=True)[:1]
        return self.filter(date__lt=Subquery(date_qs))

    def read_messages(self, user_id):
        """Returns READ messages from newest to oldest"""
        return self.exclude(unreadmessage__participant__person=user_id)

    def unread_messages(self, user_id):
        """Returns UNREAD messages from oldest to newest"""
        return (
            self.filter(unreadmessage__participant__person=user_id)
            .reverse())


class Message(models.Model):
    id = models.UUIDField(
        primary_key=True,
        default=uuid.uuid4)
    chat_room = models.ForeignKey('Chat.ChatRoom', on_delete=models.CASCADE)
    sender = models.ForeignKey(
        'Employee.User', null=True, on_delete=models.SET_NULL
    )
    text = models.TextField()
    date = models.DateTimeField(default=Now)
    objects = MessageQuerySet.as_manager()

    class Meta:
        db_table = 'chat_message'
        ordering = ('-date',)

    def __str__(self):
        dt = datetime.utcnow() if isinstance(self.date, Now) else self.date
        return f'at {dt.strftime("%Y-%-m-%-d %H:%M:%S")}, from {self.sender}'


class MessageType(models.TextChoices):
    READ = 'read', 'Read'
    UNREAD = 'unread', 'Unread'
