from django.db import models


class Participant(models.Model):
    person = models.ForeignKey('Employee.User', on_delete=models.CASCADE)
    chat_room = models.ForeignKey('Chat.ChatRoom', on_delete=models.CASCADE)

    class Meta:
        db_table = 'chat_participant'
        ordering = ('person',)
        unique_together = ('person', 'chat_room')

    def __str__(self):
        return f'{self.person} in {self.chat_room}'
