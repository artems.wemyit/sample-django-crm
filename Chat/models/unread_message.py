import uuid

from django.db import models


class UnreadMessage(models.Model):
    id = models.UUIDField(
        primary_key=True,
        default=uuid.uuid4)
    message = models.ForeignKey('Chat.Message', on_delete=models.CASCADE)
    participant = models.ForeignKey(
        'Chat.Participant', on_delete=models.CASCADE
    )

    class Meta:
        db_table = 'chat_unreadmessage'
        unique_together = ('message', 'participant')

    @classmethod
    def mark_messages_as_read(cls, user_id: int, messages_uuids: list):
        cls.objects.filter(
            participant__person=user_id,
            message__in=messages_uuids).delete()
