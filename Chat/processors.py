from django.utils.functional import SimpleLazyObject

from Chat.models.chat_room import ChatRoom


def chatroom_info(request):
    """Context processor for chat rooms info"""
    url_params = request.resolver_match.kwargs

    def chatroom_info_query():
        info = {}
        if not request.user.is_authenticated:
            return info

        info['chatrooms'] = ChatRoom.objects.filter(
            participant__person=request.user.id
        )
        info['active_chatroom'] = ChatRoom.objects.filter(
            pk=url_params.get('chat_uuid', None)).first()
        return info

    return {'chatroom_info': SimpleLazyObject(chatroom_info_query)}
