import logging
from unittest.mock import patch

from django.test import TestCase
from django.urls import reverse
from rest_framework.test import APIClient

from Chat.models.message import Message
from Chat.models.unread_message import UnreadMessage
from Chat.utils.decorators import CHAT_ACCESS_MESSAGE
from Employee.models import User


class ChatViewEndpointTestCase(TestCase):
    fixtures = ['fixtures.json']
    chat_uuid = 'd65d0970-1933-11eb-adc1-0242ac120002'
    user_without_access = 2
    user_with_access = 4

    @staticmethod
    def messages_url(chat_uuid):
        return reverse(
            'chat:chat',
            kwargs={'chat_uuid': chat_uuid})

    def login_user(self, user_id):
        self.client.force_login(user=User.objects.get(id=user_id))

    def test_redirect_unauthorized_to_login_page(self):
        url = self.messages_url(self.chat_uuid)

        resp = self.client.get(url)
        self.assertEqual(resp.status_code, 403)

        resp = self.client.post(url)
        self.assertEqual(resp.status_code, 403)

    def test_response(self):
        self.login_user(self.user_with_access)
        resp = self.client.get(self.messages_url(self.chat_uuid))

        self.assertEqual(resp.status_code, 200)
        self.assertEqual(len(resp.context_data['object_list']), 2)
        self.assertEqual(
            len(resp.context_data['object_list']['read_messages']),
            5
        )
        self.assertEqual(
            len(resp.context_data['object_list']['unread_messages']),
            5
        )

    def test_fetch_messages_from_inaccessible_chat(self):
        self.login_user(self.user_without_access)
        resp = self.client.get(self.messages_url(self.chat_uuid))
        self.assertEqual(resp.status_code, 403)


class EndpointsTestCase(TestCase):
    fixtures = ['fixtures.json']
    chat_uuid = 'd65d0970-1933-11eb-adc1-0242ac120002'
    user_without_access = 2
    user_with_access = 4

    @staticmethod
    def fetch_messages_url(chat_uuid):
        return reverse(
            'api-chat:fetch_messages',
            kwargs={'chat_uuid': chat_uuid})

    @staticmethod
    def send_message_url(chat_uuid):
        return reverse(
            'api-chat:send_message',
            kwargs={'chat_uuid': chat_uuid})

    def login_user(self, user_id):
        self.client.force_login(user=User.objects.get(id=user_id))

    def setUp(self):
        self.client = APIClient()

    def test_login_required(self):
        self.login_user(self.user_without_access)
        self.client.logout()
        response = self.client.get(
            self.fetch_messages_url(chat_uuid=self.chat_uuid))
        self.assertEqual(response.status_code, 403)
        self.assertEqual(
            response.json()['detail'],
            'Authentication credentials were not provided.')
        response = self.client.post(
            self.send_message_url(chat_uuid=self.chat_uuid))
        self.assertEqual(response.status_code, 403)
        self.assertEqual(
            response.json()['detail'],
            'Authentication credentials were not provided.')

    def test_fetch_messages_from_accessible_chat(self):
        self.login_user(self.user_with_access)
        response = self.client.get(
            self.fetch_messages_url(chat_uuid=self.chat_uuid),
            data={
                'message_type': 'read',
                'count': '10'
            })
        self.assertEqual(response.status_code, 200)

    def test_fetch_messages_from_inaccessible_chat(self):
        self.login_user(self.user_without_access)
        response = self.client.get(
            self.fetch_messages_url(chat_uuid=self.chat_uuid))
        self.assertEqual(response.status_code, 403)
        self.assertEqual(response.json()['message'], CHAT_ACCESS_MESSAGE)

    def test_fetch_messages_missing_query_params(self):
        self.login_user(self.user_with_access)
        response = self.client.get(
            self.fetch_messages_url(chat_uuid=self.chat_uuid))
        self.assertEqual(response.status_code, 400)

    def test_fetch_messages_latest_three_read(self):
        self.login_user(self.user_with_access)
        count = 3
        messages = (
            Message.objects
            .filter(chat_room=self.chat_uuid)
            .read_messages(self.user_with_access)[:count])
        expected_uuids = [str(x) for x in messages.values_list('id', flat=True)]

        response = self.client.get(
            self.fetch_messages_url(chat_uuid=self.chat_uuid),
            data={
                'message_type': 'read',
                'count': count
            })
        self.assertEqual(response.status_code, 200)

        output_uuids = [x['id'] for x in response.json()]
        self.assertEqual(output_uuids, expected_uuids)

    def test_fetch_messages_first_three_unread(self):
        self.login_user(self.user_with_access)
        count = 3
        messages = (
            Message.objects
            .filter(chat_room=self.chat_uuid)
            .unread_messages(self.user_with_access)[:count])
        expected_uuids = [str(x) for x in messages.values_list('id', flat=True)]

        response = self.client.get(
            self.fetch_messages_url(chat_uuid=self.chat_uuid),
            data={
                'message_type': 'unread',
                'count': count
            })
        self.assertEqual(response.status_code, 200)

        output_uuids = [x['id'] for x in response.json()]
        self.assertEqual(output_uuids, expected_uuids)

    def test_fetch_messages_unsupported_message_type(self):
        self.login_user(self.user_with_access)
        logging.disable(logging.CRITICAL)
        unsupported = 'unsupported'

        def mock_enum(_self, *args):
            super(_self.__class__, _self).__init__(*args)
            _self.fields['message_type'].choices.append(
                (unsupported, unsupported.title()))

        with patch('Chat.forms.FetchMessagesForm.__init__', mock_enum):
            with self.assertRaises(ValueError) as err:
                self.client.get(
                    self.fetch_messages_url(chat_uuid=self.chat_uuid),
                    data={
                        'message_type': unsupported,
                        'count': 5
                    })
        self.assertTrue('Unsupported message type' in str(err.exception))

    def test_fetch_messages_read_before_certain_message(self):
        self.login_user(self.user_with_access)
        count = 2
        before = '1c170732-3771-41f3-be18-81fb1a2e0637'
        messages = (
            Message.objects
            .filter(chat_room=self.chat_uuid)
            .read_messages(self.user_with_access)
            .messages_before_message(before)[:count])
        expected_uuids = [str(x) for x in messages.values_list('id', flat=True)]

        response = self.client.get(
            self.fetch_messages_url(chat_uuid=self.chat_uuid),
            data={
                'message_type': 'read',
                'count': count,
                'before': before
            })
        self.assertEqual(response.status_code, 200)

        output_uuids = [x['id'] for x in response.json()]
        self.assertEqual(output_uuids, expected_uuids)

    def test_fetch_messages_unread_before_certain_message(self):
        self.login_user(self.user_with_access)
        count = 2
        before = '1c170732-3771-41f3-be18-81fb1a2e0637'
        expected_output = []
        response = self.client.get(
            self.fetch_messages_url(chat_uuid=self.chat_uuid),
            data={
                'message_type': 'unread',
                'count': count,
                'before': before
            })
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json(), expected_output)

    def test_fetch_messages_unread_marked_as_read(self):
        self.login_user(self.user_with_access)
        count = 2
        response = self.client.get(
            self.fetch_messages_url(chat_uuid=self.chat_uuid),
            data={
                'message_type': 'unread',
                'count': count,
            })
        self.assertEqual(response.status_code, 200)

        output_uuids = [x['id'] for x in response.json()]
        exists = (
            UnreadMessage.objects.filter(
                participant__person=self.user_with_access,
                message__in=output_uuids)
            .exists())
        self.assertFalse(exists, msg='Fetched messages marked as read')

    def test_send_message_to_accessible_chat(self):
        self.login_user(self.user_with_access)
        response = self.client.post(
            self.send_message_url(chat_uuid=self.chat_uuid),
            data={'text': 'Test message'})
        self.assertEqual(response.status_code, 200)

    def test_send_message_to_inaccessible_chat(self):
        self.login_user(self.user_without_access)
        response = self.client.post(
            self.send_message_url(chat_uuid=self.chat_uuid),
            data={'text': 'Test message'})
        self.assertEqual(response.status_code, 403)
        self.assertEqual(response.json()['message'], CHAT_ACCESS_MESSAGE)

    def test_send_message_invalid_data(self):
        self.login_user(self.user_with_access)
        unread_messages = UnreadMessage.objects.filter(
            participant__person=self.user_with_access,
            participant__chat_room=self.chat_uuid)
        count_before = unread_messages.count()
        response = self.client.post(
            self.send_message_url(chat_uuid=self.chat_uuid))
        self.assertEqual(response.status_code, 400)
        count_after = unread_messages.count()
        self.assertEqual(count_after, count_before)

    def test_send_message_with_additional_data(self):
        self.login_user(self.user_with_access)
        another_user_id = 3
        another_chat_uuid = '36323c8f-47d1-4023-85d3-ad047d0275f8'
        text = 'Message Text'
        response = self.client.post(
            self.send_message_url(chat_uuid=self.chat_uuid),
            data={
                'text': text,
                'sender': another_user_id,
                'chat_room': another_chat_uuid
            })
        self.assertEqual(response.status_code, 200)

        new_message = Message.objects.first()
        self.assertEqual(new_message.text, text)
        self.assertEqual(new_message.sender.id, self.user_with_access)
        self.assertEqual(str(new_message.chat_room.id), self.chat_uuid)

    def test_send_message_happy_path(self):
        self.login_user(self.user_with_access)
        text = 'Test text which purpose is to test happy path'
        response = self.client.post(
            self.send_message_url(chat_uuid=self.chat_uuid),
            data={'text': text})
        self.assertEqual(response.status_code, 200)

        new_message = Message.objects.first()
        self.assertEqual(new_message.text, text)
        self.assertEqual(new_message.sender.id, self.user_with_access)
        self.assertEqual(str(new_message.chat_room.id), self.chat_uuid)
