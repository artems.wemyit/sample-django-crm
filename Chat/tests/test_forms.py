from django.conf import settings
from django.test import TestCase

from Chat.forms import FetchMessagesForm, MessageForm
from Chat.models.message import Message
from Chat.models.participant import Participant
from Chat.models.unread_message import UnreadMessage


class FetchMessagesFormTestCase(TestCase):
    def setUp(self):
        self.basic_data = {
            'message_type': 'read',
            'count': 10,
            'before': '3ec95364-1395-49fb-9965-dff9b450ef1b'
        }

    def test_message_type_read(self):
        form = FetchMessagesForm(self.basic_data)
        self.assertTrue(form.is_valid())

    def test_message_type_unread(self):
        self.basic_data['message_type'] = 'unread'
        form = FetchMessagesForm(self.basic_data)
        self.assertTrue(form.is_valid())

    def test_message_type_invalid_choice(self):
        self.basic_data['message_type'] = 'random choice'
        form = FetchMessagesForm(self.basic_data)
        self.assertFalse(form.is_valid())
        expected_errors = {
            'message_type': ['Select a valid choice. random choice is not one of the available choices.']
        }
        self.assertEqual(form.errors, expected_errors)

    def test_message_type_not_provided(self):
        del self.basic_data['message_type']
        form = FetchMessagesForm(self.basic_data)
        self.assertFalse(form.is_valid())
        expected_errors = {
            'message_type': ['This field is required.']
        }
        self.assertEqual(form.errors, expected_errors)

    def test_count_min_value(self):
        self.basic_data['count'] = settings.CHAT_CONFIGURATION['min_messages']
        form = FetchMessagesForm(self.basic_data)
        self.assertTrue(form.is_valid())

    def test_count_max_value(self):
        self.basic_data['count'] = settings.CHAT_CONFIGURATION['max_messages']
        form = FetchMessagesForm(self.basic_data)
        self.assertTrue(form.is_valid())

    def test_count_string(self):
        self.basic_data['count'] = '10'
        form = FetchMessagesForm(self.basic_data)
        self.assertTrue(form.is_valid())

    def test_count_lower_than_min(self):
        min_val = settings.CHAT_CONFIGURATION['min_messages']
        self.basic_data['count'] = min_val - 1
        form = FetchMessagesForm(self.basic_data)
        self.assertFalse(form.is_valid())
        expected_errors = {
            'count': [f'Ensure this value is greater than or equal to {min_val}.']
        }
        self.assertEqual(form.errors, expected_errors)

    def test_count_greater_that_max(self):
        max_val = settings.CHAT_CONFIGURATION['max_messages']
        self.basic_data['count'] = max_val + 1
        form = FetchMessagesForm(self.basic_data)
        self.assertFalse(form.is_valid())
        expected_errors = {
            'count': [f'Ensure this value is less than or equal to {max_val}.']
        }
        self.assertEqual(form.errors, expected_errors)

    def test_count_float(self):
        self.basic_data['count'] = 10.2
        form = FetchMessagesForm(self.basic_data)
        self.assertFalse(form.is_valid())
        expected_errors = {
            'count': ['Enter a whole number.']
        }
        self.assertEqual(form.errors, expected_errors)

    def test_count_not_provided(self):
        del self.basic_data['count']
        form = FetchMessagesForm(self.basic_data)
        self.assertFalse(form.is_valid())
        expected_errors = {
            'count': ['This field is required.']
        }
        self.assertEqual(form.errors, expected_errors)

    def test_count_none(self):
        self.basic_data['count'] = None
        form = FetchMessagesForm(self.basic_data)
        self.assertFalse(form.is_valid())
        expected_errors = {
            'count': ['This field is required.']
        }
        self.assertEqual(form.errors, expected_errors)

    def test_before_valid(self):
        form = FetchMessagesForm(self.basic_data)
        self.assertTrue(form.is_valid())

    def test_before_not_provided(self):
        del self.basic_data['before']
        form = FetchMessagesForm(self.basic_data)
        self.assertTrue(form.is_valid())

    def test_before_invalid_string(self):
        self.basic_data['before'] = 'invalid uuid'
        form = FetchMessagesForm(self.basic_data)
        self.assertFalse(form.is_valid())
        expected_errors = {'before': ['Enter a valid UUID.']}
        self.assertEqual(form.errors, expected_errors)


class MessageFormTestCase(TestCase):
    fixtures = ['fixtures.json']

    def setUp(self):
        self.basic_data = {
            'chat_room': 'd65d0970-1933-11eb-adc1-0242ac120002',
            'sender': 3,
            'text': 'valid text'
        }

    def test_data_valid(self):
        form = MessageForm(self.basic_data)
        self.assertTrue(form.is_valid())

    def test_chat_room_invalid_uuid(self):
        self.basic_data['chat_room'] = 'invalid uuid'
        form = MessageForm(self.basic_data)
        self.assertFalse(form.is_valid())
        expected_errors = {
            'chat_room': ['“invalid uuid” is not a valid UUID.']
        }
        self.assertEqual(form.errors, expected_errors)

    def test_chat_room_invalid_choice(self):
        self.basic_data['chat_room'] = '3ec95364-1395-49fb-9965-dff9b450ef1b'
        form = MessageForm(self.basic_data)
        self.assertFalse(form.is_valid())
        expected_errors = {
            'chat_room': ['Select a valid choice. That choice is not one of the available choices.']
        }
        self.assertEqual(form.errors, expected_errors)

    def test_chat_room_not_provided(self):
        del self.basic_data['chat_room']
        form = MessageForm(self.basic_data)
        self.assertFalse(form.is_valid())
        expected_errors = {
            'chat_room': ['This field is required.']
        }
        self.assertEqual(form.errors, expected_errors)

    def test_sender_invalid(self):
        self.basic_data['sender'] = 'invalid sender'
        form = MessageForm(self.basic_data)
        self.assertFalse(form.is_valid())
        expected_errors = {
            'sender': ['Select a valid choice. That choice is not one of the available choices.']
        }
        self.assertEqual(form.errors, expected_errors)

    def test_sender_not_provided(self):
        del self.basic_data['sender']
        form = MessageForm(self.basic_data)
        self.assertFalse(form.is_valid())
        expected_errors = {
            'sender': ['This field is required.']
        }
        self.assertEqual(form.errors, expected_errors)

    def test_text_not_provided(self):
        del self.basic_data['text']
        form = MessageForm(self.basic_data)
        self.assertFalse(form.is_valid())
        expected_errors = {
            'text': ['This field is required.']
        }
        self.assertEqual(form.errors, expected_errors)

    def test_text_empty(self):
        self.basic_data['text'] = ''
        form = MessageForm(self.basic_data)
        self.assertFalse(form.is_valid())
        expected_errors = {
            'text': ['This field is required.']
        }
        self.assertEqual(form.errors, expected_errors)

    def test_text_only_spaces(self):
        self.basic_data['text'] = '     '
        form = MessageForm(self.basic_data)
        self.assertFalse(form.is_valid())
        expected_errors = {
            'text': ['This field is required.']
        }
        self.assertEqual(form.errors, expected_errors)

    def test_save(self):
        form = MessageForm(self.basic_data)
        self.assertTrue(form.is_valid())

        message = form.save()
        participants = (
            Participant.objects
            .filter(chat_room=self.basic_data['chat_room'])
            .exclude(person=self.basic_data['sender']))
        unreadmessages = UnreadMessage.objects.filter(
            participant__in=participants, message=message)

        self.assertEqual(
            Message.objects.first().text,
            self.basic_data['text'],
            msg='New message was saved')
        self.assertEqual(
            participants.count(),
            unreadmessages.count(),
            msg='New message added as unread for participants except sender')

