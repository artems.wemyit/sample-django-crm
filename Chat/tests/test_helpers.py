import json
from unittest.mock import Mock, patch

from django.test import TestCase
from rest_framework import status

from Chat.utils.decorators import CHAT_ACCESS_MESSAGE, check_access_to_chat
from Chat.utils.responses import error_response


class HelpersTestCase(TestCase):
    def test_error_response(self):
        errors = {'count': ['There is an error']}
        expected_response = {
            'success': False,
            'message': {
                **errors
            }
        }
        output_response = json.loads(
            error_response(errors, 400).content.decode("utf-8"))
        self.assertEqual(output_response, expected_response)

    @patch('django.db.models.query.QuerySet.exists', Mock(return_value=True))
    def test_check_access_to_chat_accessible(self):
        user_id = 3
        chat_uuid = 'd65d0970-1933-11eb-adc1-0242ac120002'
        mock_func = Mock()
        mock_request = Mock()
        mock_request.user.id = user_id
        check_access_to_chat(mock_func)(mock_request, chat_uuid)
        mock_func.assert_called_once_with(mock_request, chat_uuid)

    @patch('Chat.utils.responses.error_response')
    def test_check_access_to_chat_inaccessible(self, mock_return_error):
        user_id = 1
        chat_uuid = 'd65d0970-1933-11eb-adc1-0242ac120002'
        mock_func = Mock()
        mock_request = Mock()
        mock_request.user.id = user_id
        check_access_to_chat(mock_func)(mock_request, chat_uuid)
        mock_return_error.assert_called_once_with(
            CHAT_ACCESS_MESSAGE, status.HTTP_403_FORBIDDEN)
