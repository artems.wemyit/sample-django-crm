from django.db.models import Max, Min
from django.test import TestCase

from Chat.models.message import Message
from Chat.models.participant import Participant
from Chat.models.unread_message import UnreadMessage
from Employee.models import User


class UnreadMessageTestCase(TestCase):
    fixtures = ['fixtures.json']
    user_id = 3
    chat_uuid = 'd65d0970-1933-11eb-adc1-0242ac120002'

    def setUp(self):
        self.user = User.objects.get(id=self.user_id)
        self.participant = Participant.objects.get(
            person=self.user, chat_room=self.chat_uuid)
        self.user_unreadmessages = (
            UnreadMessage.objects.filter(participant=self.participant))

    def test_mark_messages_as_read_empty_uuids(self):
        test_messages_uuids = []
        expected_unreadmessages = list(self.user_unreadmessages.values_list('id', flat=True))
        UnreadMessage.mark_messages_as_read(
            user_id=self.user.id,
            messages_uuids=test_messages_uuids)
        output_unreadmessages = list(self.user_unreadmessages.values_list('id', flat=True))
        self.assertEqual(output_unreadmessages, expected_unreadmessages)

    def test_mark_messages_as_read_uuids_not_exist(self):
        not_exist_uuids = list(
            Message.objects.exclude(
                unreadmessage__participant=self.participant
            ).values_list('id', flat=True))
        expected_unreadmessages = list(self.user_unreadmessages.values_list('id', flat=True))
        UnreadMessage.mark_messages_as_read(
            user_id=self.user.id,
            messages_uuids=not_exist_uuids)
        output_unreadmessages = list(self.user_unreadmessages.values_list('id', flat=True))
        self.assertEqual(output_unreadmessages, expected_unreadmessages)

    def test_mark_messages_as_read_multiple_uuids(self):
        unread_uuids = list(self.user_unreadmessages[:5].values_list('message_id'))
        expected_unreadmessages = list(
            self.user_unreadmessages
            .exclude(message__in=unread_uuids)
            .values_list('id', flat=True))
        UnreadMessage.mark_messages_as_read(
            user_id=self.user.id,
            messages_uuids=unread_uuids)
        output_unreadmessages = list(self.user_unreadmessages.values_list('id', flat=True))
        self.assertEqual(output_unreadmessages, expected_unreadmessages)


class MessageTestCase(TestCase):
    fixtures = ['fixtures.json']

    def test_chat_messages_empty_chat(self):
        chat_with_ten_messages = '36323c8f-47d1-4023-85d3-ad047d0275f8'
        expected_messages = []
        output_messages = Message.objects.chat_messages(chat_with_ten_messages)
        self.assertEqual(list(output_messages), expected_messages)

    def test_chat_messages_get_all(self):
        chat_without_messages = 'd65d0970-1933-11eb-adc1-0242ac120002'
        expected_messages = list(Message.objects.filter(chat_room=chat_without_messages))
        output_messages = Message.objects.chat_messages(chat_without_messages)
        self.assertEqual(list(output_messages), expected_messages)

    def test_messages_before_first_message(self):
        message = Message.objects.first()
        expected_messages = Message.objects.all()[1:]
        output_messages = Message.objects.messages_before_message(message.id)
        self.assertEqual(
            list(output_messages.values_list('id', flat=True)),
            list(expected_messages.values_list('id', flat=True)),
            msg='Returns all messages except first')

    def test_messages_before_last_message(self):
        message = Message.objects.last()
        expected_messages = []
        output_messages = Message.objects.messages_before_message(message.id)
        self.assertEqual(
            list(output_messages.values_list('id', flat=True)),
            expected_messages)

    def test_read_messages_empty(self):
        user_all_unread = 3
        expected_messages = []
        output_messages = Message.objects.read_messages(user_all_unread)
        self.assertEqual(list(output_messages), expected_messages)

    def test_read_messages_partially_read(self):
        user_partially_read = 4
        expected_messages = list(Message.objects.exclude(
            unreadmessage__participant__person=user_partially_read))
        output_messages = Message.objects.read_messages(user_partially_read)
        self.assertEqual(list(output_messages), expected_messages)

    def test_read_messages_all_read(self):
        user_all_read = 5
        expected_messages = list(Message.objects.exclude(
            unreadmessage__participant__person=user_all_read))
        output_messages = Message.objects.read_messages(user_all_read)
        self.assertEqual(list(output_messages), expected_messages)

    def test_read_messages_from_newest_to_oldest(self):
        user_all_read = 5
        output_messages = Message.objects.read_messages(user_all_read)
        self.assertEqual(
            output_messages.first().date,
            output_messages.aggregate(Max('date'))['date__max'])
        self.assertEqual(
            output_messages.last().date,
            output_messages.aggregate(Min('date'))['date__min'])

    def test_unread_messages_empty(self):
        # User which has all message marked as read
        user_all_read = 5
        expected_messages = []
        output_messages = Message.objects.unread_messages(user_all_read)
        self.assertEqual(list(output_messages), expected_messages)

    def test_unread_messages_partially_unread(self):
        user_partially_read = 4
        expected_messages = list(Message.objects.filter(
            unreadmessage__participant__person=user_partially_read).reverse())
        output_messages = Message.objects.unread_messages(user_partially_read)
        self.assertEqual(list(output_messages), expected_messages)

    def test_unread_messages_all_unread(self):
        user_all_unread = 3
        expected_messages = list(Message.objects.filter(
            unreadmessage__participant__person=user_all_unread).reverse())
        output_messages = Message.objects.unread_messages(user_all_unread)
        self.assertEqual(list(output_messages), expected_messages)

    def test_unread_messages_from_oldest_to_newest(self):
        user_all_unread = 3
        output_messages = Message.objects.unread_messages(user_all_unread)
        self.assertEqual(
            output_messages.first().date,
            output_messages.aggregate(Min('date'))['date__min'])
        self.assertEqual(
            output_messages.last().date,
            output_messages.aggregate(Max('date'))['date__max'])
