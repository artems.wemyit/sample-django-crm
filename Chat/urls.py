from django.urls import path

from Chat.views import ChatView


app_name = 'chat'

urlpatterns = [
    path('<uuid:chat_uuid>/', ChatView.as_view(), name='chat'),
]
