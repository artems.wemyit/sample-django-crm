from django.utils.translation import ugettext as _
from rest_framework import status

from Chat.models.participant import Participant
from Chat.utils.responses import error_response

CHAT_ACCESS_MESSAGE = "You don't have access to this chat or it doesn't exist"


def check_access_to_chat(func):
    def decorator(request, chat_uuid):
        access = Participant.objects.filter(
            chat_room=chat_uuid, person=request.user.id).exists()
        if access:
            return func(request, chat_uuid)
        return error_response(
            _(CHAT_ACCESS_MESSAGE), status.HTTP_403_FORBIDDEN
        )
    return decorator
