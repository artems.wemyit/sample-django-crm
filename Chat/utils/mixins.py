from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponseForbidden

from Chat.models.participant import Participant


class ChatAccessMixin(LoginRequiredMixin):
    def dispatch(self, request, *args, **kwargs):
        chat_uuid = self.kwargs.get('chat_uuid')
        access = Participant.objects.filter(
            chat_room=chat_uuid, person=request.user.id).exists()
        if access:
            return super().dispatch(request, *args, **kwargs)
        return HttpResponseForbidden()
