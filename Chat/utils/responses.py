from django.http import JsonResponse


def error_response(message, status_code):
    return JsonResponse(
        {
            'success': False,
            'message': message
        }, status=status_code)
