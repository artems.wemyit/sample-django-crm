from django.conf import settings
from django.http import JsonResponse
from django.shortcuts import redirect
from django.views.generic import ListView
from rest_framework import permissions
from rest_framework import status
from rest_framework.decorators import api_view, permission_classes

from Chat.forms import FetchMessagesForm, MessageForm
from Chat.models.message import Message, MessageType
from Chat.models.unread_message import UnreadMessage
from Chat.serializers import MessageSerializer
from Chat.utils.decorators import check_access_to_chat
from Chat.utils.mixins import ChatAccessMixin
from Chat.utils.responses import error_response


@api_view(['GET'])
@check_access_to_chat
@permission_classes((permissions.IsAuthenticated,))
def fetch_messages(request, chat_uuid):
    form = FetchMessagesForm(request.GET)
    if not form.is_valid():
        return error_response(form.errors, status.HTTP_400_BAD_REQUEST)

    messages = Message.objects.chat_messages(chat_uuid)
    user = request.user
    data = form.cleaned_data
    if data['message_type'] == MessageType.READ:
        messages = messages.read_messages(user.id)
    elif data['message_type'] == MessageType.UNREAD:
        messages = messages.unread_messages(user.id)
    else:
        raise ValueError(f"Unsupported message type {data['message_type']}")

    before_message_uuid = data['before']
    if before_message_uuid is not None:
        messages = messages.messages_before_message(before_message_uuid)

    result = list(messages[:data['count']])
    UnreadMessage.mark_messages_as_read(user.id, [x.id for x in result])

    serializer = MessageSerializer(result, many=True)
    return JsonResponse(serializer.data, safe=False)


@api_view(['POST'])
@check_access_to_chat
@permission_classes((permissions.IsAuthenticated,))
def send_message(request, chat_uuid):
    form = MessageForm({
        **request.POST.dict(),
        'sender': request.user.id,
        'chat_room': chat_uuid
    })
    if not form.is_valid():
        return error_response(form.errors, status.HTTP_400_BAD_REQUEST)

    form.save()
    return JsonResponse({'success': True})


class ChatView(ChatAccessMixin, ListView):
    template_name = 'Chat/chat.html'

    def get_queryset(self):
        msg_count = settings.CHAT_CONFIGURATION['max_messages']
        chat_uuid = self.kwargs.get('chat_uuid')
        user = self.request.user
        messages = Message.objects.chat_messages(chat_uuid)
        read_messages = list(messages.read_messages(user.id)[:msg_count])
        unread_messages = list(messages.unread_messages(user.id)[:msg_count])
        UnreadMessage.mark_messages_as_read(
            user.id,
            [x.id for x in unread_messages]
        )
        return dict(
            read_messages=read_messages,
            unread_messages=unread_messages
        )

    def post(self, request, *args, **kwargs):
        chat_uuid = kwargs['chat_uuid']
        form = MessageForm({
            **request.POST.dict(),
            'sender': request.user.id,
            'chat_room': chat_uuid
        })
        if not form.is_valid():
            return error_response(form.errors, status.HTTP_400_BAD_REQUEST)
        form.save()
        return redirect('chat:chat', chat_uuid)
